package javacoursework;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PersonTest extends junit.framework.TestCase{
    Person instance;
    
    public PersonTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        instance = new Person(101, "Jesney", "35 Haseldine", "1234567890");
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testGetIDNumber() {
        System.out.println("getIDNumber");
        int result = instance.getIDNumber();
        assertEquals(101, result);
    }

    @Test
    public void testGetFullName() {
        System.out.println("getFullName");
        String result = instance.getFullName();
        assertEquals("Jesney", result);
    }

    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        String result = instance.getAddress();
        assertEquals("35 Haseldine", result);
    }

    @Test
    public void testGetTelephoneNumber() {
        System.out.println("getTelephoneNumber");
        String result = instance.getTelephoneNumber();
        assertEquals("1234567890", result);
    }
    
}
