package javacoursework;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExpertiseTest extends junit.framework.TestCase {
    Expertise expertise;
    Treatment treatment;
    Room room;

    public ExpertiseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        room = new Room("Medical consulting Suit 1");
        treatment = new Treatment("Consulting", room);
        expertise = new Expertise(201, "Physiotherapy", treatment);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetIDNumber() {
        System.out.println("getIDNumber");
        int result = expertise.getIDNumber();
        assertEquals(201, result);
    }

    @Test
    public void testGetTreatment() {
        System.out.println("getTreatment");
        Treatment result = expertise.getTreatment();
        assertEquals(treatment, result);
    }
}
