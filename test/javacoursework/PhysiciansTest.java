package javacoursework;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PhysiciansTest extends junit.framework.TestCase {
    Physicians physician;
    Appointment appointment;
    Treatment treatment;
    Room room;
    Person patient;
    Expertise expertise;

    public PhysiciansTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        physician = new Physicians(1001, "Jesney Johny", "35, Haseldine Meadows, Hatfield-AL10 8HA", "123456789");
        room = new Room("Medical consulting Suit 1");
        treatment = new Treatment("Consulting", room);
        appointment = new Appointment(301, "2021-05-21", "12:00-12:30");
        patient = new Person(101, "Jesney", "35 Haseldine", "1234567890");
        expertise = new Expertise(206, "Rehabiltation", treatment);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddAppointment() {
        System.out.println("addAppointment");
        physician.addAppointment(appointment);
        ArrayList<Appointment> result = physician.getAppointment();
        assert(result.contains(appointment));
    }

    @Test
    public void testAddExpertise() {
        System.out.println("addExpertise");
        physician.addExpertise(expertise);
        ArrayList<Expertise> result = physician.getExpertise();
        assert(result.contains(expertise));
    }

    @Test
    public void testGetExpertise() {
        System.out.println("getExpertise");
        physician.addExpertise(expertise);
        ArrayList<Expertise> result = physician.getExpertise();
        assert(result.contains(expertise));
    }

    @Test
    public void testGetAppointment() {
        System.out.println("getAppointment");
        physician.addAppointment(appointment);
        ArrayList<Appointment> result = physician.getAppointment();
        assert(result.contains(appointment));
    }
    
}
