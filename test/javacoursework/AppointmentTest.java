package javacoursework;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AppointmentTest extends junit.framework.TestCase {
    Appointment appointment;
    Treatment treatment;
    Room room;
    Person patient;
    
    public AppointmentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        room = new Room("Medical consulting Suit 1");
        treatment = new Treatment("Consulting", room);
        appointment = new Appointment(301, "2021-05-21", "12:00-12:30");
        patient = new Person(101, "Jesney", "35 Haseldine", "1234567890");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetIDNumber() {
        System.out.println("getIDNumber");
        int result = appointment.getIDNumber();
        assertEquals(301, result);
    }

    @Test
    public void testGetAvailability() {
        System.out.println("getAvailability");
        String result = appointment.getAvailability();
        assertEquals("Yes", result);
    }

    @Test
    public void testGetTreatment() {
        System.out.println("getTreatment");
        appointment.setTreatment(treatment);
        Treatment result = appointment.getTreatment();
        assertEquals(treatment, result);
    }

    @Test
    public void testGetPatient() {
        System.out.println("getPatient");
        appointment.setPatient(patient);
        Person result = appointment.getPatient();
        assertEquals(patient, result);
    }

    @Test
    public void testSetPatient() {
        System.out.println("setPatient");
        appointment.setPatient(patient);
        Person result = appointment.getPatient();
        assertEquals(patient, result);
    }

    @Test
    public void testSetAvailability() {
        System.out.println("setAvailability");
        appointment.setAvailability("No");
        String result = appointment.getAvailability();
        assertEquals("No", result);
    }

    @Test
    public void testSetTreatment() {
        System.out.println("setTreatment");
        appointment.setTreatment(treatment);
        Treatment result = appointment.getTreatment();
        assertEquals(treatment, result);
    }
}
