package javacoursework;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TreatmentTest extends junit.framework.TestCase {
    Treatment treatment;
    Room room;
    
    public TreatmentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        room = new Room("Medical consulting Suit 1");
        treatment = new Treatment("Consulting", room);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetName() {
        System.out.println("getName");
        String result = treatment.getName();
        assertEquals("Consulting", result);
    }
    
}
