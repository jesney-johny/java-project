package javacoursework;
import java.util.Random;
import java.util.Scanner;

public class TestCoursework {

    public static void main(String args[]) {
        Database database = new Database();
        Random rand = new Random();
        
        Room r1 = new Room("Medical consulting Suit 1");
        Room r2 = new Room("Medical consulting Suit 2");
        Room r3 = new Room("Medical consulting Suit 3");
        Room r4 = new Room("Swimming pool");
        Room r5 = new Room("Gym");

        Treatment t1 = new Treatment("Consulting", r1);
        Treatment t2 = new Treatment("Neural Mobilisation", r4);
        Treatment t3 = new Treatment("Acupuncture", r5);
        Treatment t4 = new Treatment("Massage", r5);
        Treatment t5 = new Treatment("Mobilisation of the Spineand Joints", r2);
        Treatment t6 = new Treatment("Pool rehabiltation", r3);
        
        Expertise e1 = new Expertise(201, "Physiotherapy", t4);
        database.addExpertise(e1);
        
        Expertise e2 = new Expertise(202, "Osteopathy", t2);
        database.addExpertise(e2);
        
        Expertise e3 = new Expertise(203, "Rehabiltation", t3);
        database.addExpertise(e3);
        
        Expertise e4 = new Expertise(204, "Consulting", t1);
        database.addExpertise(e4);
        
        Expertise e5 = new Expertise(205, "Rehabiltation", t5);
        database.addExpertise(e5);
        
        Expertise e6 = new Expertise(206, "Rehabiltation", t6);
        database.addExpertise(e6);
        
        Physicians p1 = new Physicians(1001, "Jesney Johny", "35, Haseldine Meadows, Hatfield-AL10 8HA", "123456789");
        p1.addExpertise(e1);
        p1.addExpertise(e2);
        p1.addExpertise(e4);
        Appointment a = new Appointment(301, "2021-05-21", "12:00-12:30");
        p1.addAppointment(a);
        Appointment b = new Appointment(302, "2021-05-11", "12:00-12:30");
        p1.addAppointment(b);
        Appointment c = new Appointment(303,"2021-05-01", "12:00-12:30");
        p1.addAppointment(c);
        database.addPhysician(p1);
        
        Physicians p2 = new Physicians(1002, "Elanor Michael", "12, Haseldine Meadows, Hatfield-AL10 8HA", "895456789");
        p2.addExpertise(e3);
        p2.addExpertise(e4);
        p2.addExpertise(e5);
        Appointment d = new Appointment(304, "2021-05-03", "10:00-10:30");
        p2.addAppointment(d);
        Appointment e = new Appointment(305, "2021-05-13", "10:00-10:30");
        p2.addAppointment(e);
        Appointment f = new Appointment(306, "2021-05-12", "10:00-10:30");
        p2.addAppointment(f);
        database.addPhysician(p2);
        
        Physicians p3 = new Physicians(1003, "Tahani Al-Jamal", "07, Haseldine Meadows, Hatfield-AL10 8HA", "889566789");
        p3.addExpertise(e6);
        p3.addExpertise(e4);
        p3.addExpertise(e1);
        Appointment g = new Appointment(307, "2021-05-07", "10:00-10:30");
        p3.addAppointment(g);
        Appointment h = new Appointment(308, "2021-05-17", "10:00-10:30");
        p3.addAppointment(h);
        Appointment i = new Appointment(309, "2021-05-22", "10:00-10:30");
        p3.addAppointment(i);
        database.addPhysician(p3);
        
        Physicians p4 = new Physicians(1004, "Chidii", "07, Willow Way, Hatfield-AL10 8HA", "777766789");
        p4.addExpertise(e2);
        p4.addExpertise(e4);
        p4.addExpertise(e3);
        Appointment j = new Appointment(310, "2021-05-08", "15:00-15:30");
        p4.addAppointment(j);
        Appointment k = new Appointment(311, "2021-05-27", "15:00-15:30");
        p4.addAppointment(k);
        Appointment l = new Appointment(312, "2021-05-23", "15:00-15:30");
        p4.addAppointment(l);
        database.addPhysician(p4);
        
        Physicians p5 = new Physicians(1005, "Stephan King", "07, Pasture Way, Hatfield-AL10 8HA", "778966789");
        p5.addExpertise(e5);
        p5.addExpertise(e6);
        p5.addExpertise(e4);
        Appointment m = new Appointment(313, "2021-05-04", "16:00-16:30");
        p5.addAppointment(m);
        Appointment n = new Appointment(314, "2021-05-17", "16:00-16:30");
        p5.addAppointment(n);
        Appointment o = new Appointment(316, "2021-05-14", "16:00-16:30");
        p5.addAppointment(o);
        database.addPhysician(p5);        
        
        Person per1 = new Person(1006, "Lucy", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per1);
        
        Person per2 = new Person(1007, "Michael", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per2);
        
        Person per3 = new Person(1008, "Sanjana", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per3);
        
        Person per4 = new Person(1009, "Sasha", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per4);
        
        Person per5 = new Person(1010, "Sachin", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per5);
        
        Person per6 = new Person(1011, "Mike", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per6);
        
        Person per7 = new Person(1012, "Karan", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per7);
        
        Person per8 = new Person(1013, "Ritu", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per8);
        
        Person per9 = new Person(1014, "Reshma", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per9);
        
        Person per10 = new Person(1015, "Alina", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per10);
        
        Person per11 = new Person(1016, "Jestin", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per11);
        
        Person per12 = new Person(1017, "Johny", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per12);
        
        Person per13 = new Person(1018, "Tinu", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per13);
        
        Person per14 = new Person(1019, "Rachel", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per14);
        
        Person per15 = new Person(1020, "Tony", "07, Random street, Hatfield-AL10 8HA", "778966789");
        database.addPatient(per15);
       
        boolean exit = false;
        Scanner s = new Scanner(System.in);

        do {
            System.out.println("");
            System.out.println("**********************************************************************************************");
            System.out.println("");
            System.out.println("1. Are you a patient? ");
            System.out.println("2. Are you a vistor? ");
            System.out.println("3. New patient? ");
            System.out.println("4. Exit");
            System.out.println("Enter your option number: ");
            
            int option1 = s.nextInt();
            
            if (option1 == 4) {
                exit = true;
            } else if (option1 == 1) {
                System.out.println("");
                System.out.println("**********************************************************************************************");
                System.out.println("");
                System.out.println("Enter your patient ID: ");
                
                int patientId = s.nextInt();
                int patientIndex = database.checkPatientId(patientId);
                
                if (patientIndex < 0) {
                    System.out.println("Sorry no record found. Please try again!");
                } else {
                    Person currentPatient = database.getPatientByIndex(patientIndex);
                    
                    System.out.println("");
                    System.out.println("**********************************************************************************************");
                    System.out.println("");
                    System.out.println("Your details: ");
                    
                    currentPatient.toPrint();
                    
                    System.out.println("");
                    System.out.println("**********************************************************************************************");
                    System.out.println("");
                    System.out.println("1. Search physician by expertise ");
                    System.out.println("2. Search physician by name ");
                    System.out.println("3. Attend an appointment ");
                    System.out.println("4. Cancel an appointment ");
                    System.out.println("5. Exit");
                    System.out.println("Enter your option number: ");
                    
                    int option2 = s.nextInt();
                    
                    if (option2 == 5) {
                        exit = true;
                    } else if (option2 == 3) {
                        System.out.println("");
                        System.out.println("**********************************************************************************************");
                        System.out.println("");
                        System.out.println("These are the appointments made by you: ");
                        database.listAppointmentByPatient(currentPatient.getIDNumber());
                        System.out.println("Enter the appointment ID to attend");
                        int appointmentId = s.nextInt();   
                        int appointmentIndex = database.checkAppointmentById(appointmentId);
                        if (appointmentIndex < 0) {
                            System.out.println("Sorry no appointment found with that ID. Please try again!");
                        } else {
                            System.out.println("You have attended the appointment!");
                        }
                    } else if (option2 == 4) {
                        System.out.println("");
                        System.out.println("**********************************************************************************************");
                        System.out.println("");
                        System.out.println("These are the appointments made by you: ");
                        database.listAppointmentByPatient(currentPatient.getIDNumber());
                        System.out.println("Enter the appointment ID to attend");
                        int appointmentId = s.nextInt();   
                        int appointmentIndex = database.cancelAppointment(appointmentId);
                        if (appointmentIndex < 0) {
                            System.out.println("Sorry no appointment found with that ID. Please try again!");
                        } else {
                            System.out.println("You have cancelled the appointment!");
                        }
                    } else if (option2 == 1) {
                        database.listExpertise();
                        
                        System.out.println("");
                        System.out.println("**********************************************************************************************");
                        System.out.println("");
                        System.out.println("Enter the expertise ID: ");
                        
                        int expertiseId = s.nextInt();
                        int expertiseIndex = database.checkExpertiseId(expertiseId);
                        
                        if (expertiseIndex < 0) {
                            System.out.println("Sorry no expertise found with that ID. Please try again!");
                        } else {
                            Expertise currentExpertise = database.getExpertiseByIndex(expertiseIndex);
                            
                            System.out.println("");
                            System.out.println("**********************************************************************************************");
                            System.out.println("");
                            System.out.println("The expertise selected by you: ");
                            
                            currentExpertise.toPrint();
                        
                            System.out.println("");
                            System.out.println("**********************************************************************************************");
                            System.out.println("");
                            System.out.println("Physicians having these expertise are: ");
                            
                            database.getPhysiciansByExpertise(currentExpertise);
                            
                            System.out.println("");
                            System.out.println("**********************************************************************************************");
                            System.out.println("");
                            System.out.println("Enter the physicians ID number to continue");
                            
                            int physicianId = s.nextInt();
                            int physicianIndex = database.checkPhysicianId(physicianId);
                            
                            if (physicianIndex < 0) {
                                System.out.println("Sorry no physician found with that ID. Please try again!");
                            } else {
                                Physicians currentPhysician = database.getPhysicianByIndex(physicianIndex);
                                
                                System.out.println("");
                                System.out.println("**********************************************************************************************");
                                System.out.println("");
                                System.out.println("The physician selected by you: ");
                                
                                currentPhysician.print();
                                
                                System.out.println("");
                                System.out.println("**********************************************************************************************");
                                System.out.println("");
                                System.out.println("Enter the appointment number to continue: ");
                                
                                int appointmentId = s.nextInt();   
                                int appointmentIndex = database.checkAppointmentId(appointmentId, currentPhysician);
                                if (appointmentIndex < 0) {
                                    System.out.println("Sorry no appointment found with that ID. Please try again!");
                                } else {
                                    Appointment currentAppointment = database.getAppointmentByIndex(appointmentIndex, currentPhysician);
                                    
                                    System.out.println("");
                                    System.out.println("**********************************************************************************************");
                                    System.out.println("");
                                    System.out.println("The appointmnet selected by you: ");
                                    
                                    currentAppointment.toPrint();
                                    
                                    if (currentAppointment.getAvailability().equals("No")) {
                                        System.out.println("Sorry the appointment has already been booked. Please try again");
                                    } else {
                                        System.out.println("");
                                        System.out.println("**********************************************************************************************");
                                        System.out.println("");
                                        System.out.println("Do you wish to book this appointment? ");
                                        System.out.println("1. Yes");
                                        System.out.println("2. No");
                                        System.out.println("Enter your option number: ");
                                        
                                        int option3 = s.nextInt();
                                        
                                        if (option3 == 2) {
                                            exit = true;
                                        } else if (option3 == 1) {
                                            currentAppointment.setPatient(currentPatient);
                                            currentAppointment.setAvailability("No");
                                            currentAppointment.setTreatment(currentExpertise.getTreatment());
                                            currentPhysician.getAppointment().set(appointmentIndex, currentAppointment);
                                            
                                            System.out.println("");
                                            System.out.println("**********************************************************************************************");
                                            System.out.println("");
                                            System.out.println("Your appointment has been booked successfully!");
                                        } else {
                                            System.out.println("Sorry wrong option entered. Please try agaon!");
                                        }
                                    }
                                    
                                }
                            }
                        }
                    } else if (option2 == 2) {
                        System.out.println("");
                        System.out.println("**********************************************************************************************");
                        System.out.println("");
                        
                        database.listPhysician();
                        System.out.println("Enter the physicians ID number to continue");    
                        int physicianId = s.nextInt();
                        int physicianIndex = database.checkPhysicianId(physicianId);
                            
                         if (physicianIndex < 0) {
                            System.out.println("Sorry no physician found with that ID. Please try again!");
                        } else {
                            Physicians currentPhysician = database.getPhysicianByIndex(physicianIndex);
                               
                            System.out.println("");
                            System.out.println("**********************************************************************************************");
                            System.out.println("");
                            System.out.println("The physician selected by you: ");
                                
                            currentPhysician.print();
                               
                            System.out.println("");
                            System.out.println("**********************************************************************************************");
                            System.out.println("");
                            System.out.println("Enter the appointment number to continue: ");
                                
                            int appointmentId = s.nextInt();   
                            int appointmentIndex = database.checkAppointmentId(appointmentId, currentPhysician);
                            if (appointmentIndex < 0) {
                                System.out.println("Sorry no appointment found with that ID. Please try again!");
                            } else {
                                Appointment currentAppointment = database.getAppointmentByIndex(appointmentIndex, currentPhysician);
                                    
                                System.out.println("");
                                System.out.println("**********************************************************************************************");
                                System.out.println("");
                                System.out.println("The appointmnet selected by you: ");
                                    
                                currentAppointment.toPrint();
                                 
                                if (currentAppointment.getAvailability().equals("No")) {
                                    System.out.println("Sorry the appointment has already been booked. Please try again");
                                } else {
                                    System.out.println("");
                                    System.out.println("**********************************************************************************************");
                                    System.out.println("");
                                    System.out.println("Do you wish to book this appointment? ");
                                    System.out.println("1. Yes");
                                    System.out.println("2. No");
                                    System.out.println("Enter your option number: ");
                                       
                                    int option3 = s.nextInt();
                                     
                                    if (option3 == 2) {
                                        exit = true;
                                    } else if (option3 == 1) {
                                        currentAppointment.setPatient(currentPatient);
                                        currentAppointment.setAvailability("No");
                                        currentAppointment.setTreatment(currentPhysician.getExpertise().get(0).getTreatment());
                                        currentPhysician.getAppointment().set(appointmentIndex, currentAppointment);
                                            
                                        System.out.println("");
                                        System.out.println("**********************************************************************************************");
                                        System.out.println("");
                                        System.out.println("Your appointment has been booked successfully!");
                                    } else {
                                        System.out.println("Sorry wrong option selected. Please try again!");
                                    }
                                }
                                    
                            }
                         }
                    } else {
                        System.out.println("Sorry wrong option entered. Please try again!");
                    }                
                }
            } else if(option1 == 2) {
                System.out.println("");
                System.out.println("**********************************************************************************************");
                System.out.println("");
                System.out.println("Please enter your name to continue: ");
                
                s.nextLine();
                String name = s.nextLine();
                
                System.out.println("");
                System.out.println("**********************************************************************************************");
                System.out.println("");
                System.out.println("1. Search physician by expertise ");
                System.out.println("2. Search physician by name ");
                System.out.println("3. Exit");
                System.out.println("Enter your option number: ");
                
                int option2 = s.nextInt();
                    
                if (option2 == 3) {
                    exit = true;
                } else if (option2 == 1) {
                    database.listExpertise();
                        
                    System.out.println("");
                    System.out.println("**********************************************************************************************");
                    System.out.println("");
                    System.out.println("Enter the expertise ID: ");
                        
                    int expertiseId = s.nextInt();
                    int expertiseIndex = database.checkExpertiseId(expertiseId);
                        
                    if (expertiseIndex < 0) {
                        System.out.println("Sorry no expertise found with that ID. Please try again!");
                    } else {
                        Expertise currentExpertise = database.getExpertiseByIndex(expertiseIndex);
                            
                        System.out.println("");
                        System.out.println("**********************************************************************************************");
                        System.out.println("");
                        System.out.println("The expertise selected by you: ");
                            
                        currentExpertise.toPrint();
                        
                        System.out.println("");
                        System.out.println("**********************************************************************************************");
                        System.out.println("");
                        System.out.println("Physicians having these expertise are: ");
                            
                        database.getPhysiciansByExpertise(currentExpertise);
                            
                        System.out.println("");
                        System.out.println("**********************************************************************************************");
                        System.out.println("");
                        System.out.println("Enter the physicians ID number to continue");
                            
                        int physicianId = s.nextInt();
                        int physicianIndex = database.checkPhysicianId(physicianId);
                            
                        if (physicianIndex < 0) {
                            System.out.println("Sorry no physician found with that ID. Please try again!");
                        } else {
                            Physicians currentPhysician = database.getPhysicianByIndex(physicianIndex);
                                
                            System.out.println("");
                            System.out.println("**********************************************************************************************");
                            System.out.println("");
                            System.out.println("The physician selected by you: ");
                                
                            currentPhysician.print();
                                
                            System.out.println("");
                            System.out.println("**********************************************************************************************");
                            System.out.println("");
                            System.out.println("Enter the consulting time number to continue: ");
                                
                            int appointmentId = s.nextInt();   
                            int appointmentIndex = database.checkAppointmentId(appointmentId, currentPhysician);
                            
                            if (appointmentIndex < 0) {
                                System.out.println("Sorry no consulting time found with that ID. Please try again!");
                            } else {
                                Appointment currentAppointment = database.getAppointmentByIndex(appointmentIndex, currentPhysician);
                                    
                                System.out.println("");
                                System.out.println("**********************************************************************************************");
                                System.out.println("");
                                System.out.println("The consulting time selected by you: ");
                                    
                                currentAppointment.toPrint();
                                    
                                if (currentAppointment.getAvailability().equals("No")) {
                                    System.out.println("Sorry the appointment has already been booked. Please try again");
                                } else {
                                    System.out.println("");
                                    System.out.println("**********************************************************************************************");
                                    System.out.println("");
                                    System.out.println("Do you wish to book this consulting time? ");
                                    System.out.println("1. Yes");
                                    System.out.println("2. No");
                                    System.out.println("Enter your option number: ");
                                        
                                    int option3 = s.nextInt();
                                        
                                    if (option3 == 2) {
                                        exit = true;
                                    } else if (option3 == 1) {
                                        Person person = new Person(name);
                                        currentAppointment.setPatient(person);
                                        currentAppointment.setAvailability("No");
                                        currentAppointment.setTreatment(e4.getTreatment());
                                        currentPhysician.getAppointment().set(appointmentIndex, currentAppointment);
                                            
                                        System.out.println("");
                                        System.out.println("**********************************************************************************************");
                                        System.out.println("");
                                        System.out.println("Your consulting time has been booked successfully!");
                                    } else {
                                        System.out.println("Sorry wrong option entered. Please try agaon!");
                                    }
                                }
                            }
                        }
                    }
                } else if (option2 == 2) {
                    System.out.println("");
                    System.out.println("**********************************************************************************************");
                    System.out.println("");
                        
                    database.listPhysician();
                    System.out.println("Enter the physicians ID number to continue");    
                    int physicianId = s.nextInt();
                    int physicianIndex = database.checkPhysicianId(physicianId);
                            
                    if (physicianIndex < 0) {
                        System.out.println("Sorry no physician found with that ID. Please try again!");
                    } else {
                        Physicians currentPhysician = database.getPhysicianByIndex(physicianIndex);
                               
                        System.out.println("");
                        System.out.println("**********************************************************************************************");
                        System.out.println("");
                        System.out.println("The physician selected by you: ");
                                
                        currentPhysician.print();
                               
                        System.out.println("");
                        System.out.println("**********************************************************************************************");
                        System.out.println("");
                        System.out.println("Enter the consultancy time number to continue: ");
                                
                        int appointmentId = s.nextInt();   
                        int appointmentIndex = database.checkAppointmentId(appointmentId, currentPhysician);
                        
                        if (appointmentIndex < 0) {
                            System.out.println("Sorry no consultancy time found with that ID. Please try again!");
                        } else {
                            Appointment currentAppointment = database.getAppointmentByIndex(appointmentIndex, currentPhysician);
                                    
                            System.out.println("");
                            System.out.println("**********************************************************************************************");
                            System.out.println("");
                            System.out.println("The consultancy time selected by you: ");
                                    
                            currentAppointment.toPrint();
                                 
                            if (currentAppointment.getAvailability().equals("No")) {
                                System.out.println("Sorry the consultancy time has already been booked. Please try again");
                            } else {
                                System.out.println("");
                                System.out.println("**********************************************************************************************");
                                System.out.println("");
                                System.out.println("Do you wish to book this consultancy time? ");
                                System.out.println("1. Yes");
                                System.out.println("2. No");
                                System.out.println("Enter your option number: ");
                                       
                                int option3 = s.nextInt();
                                     
                                if (option3 == 2) {
                                    exit = true;
                                } else if (option3 == 1) {
                                    Person patient = new Person(name);
                                    currentAppointment.setPatient(patient);
                                    currentAppointment.setAvailability("No");
                                    currentAppointment.setTreatment(e4.getTreatment());
                                    currentPhysician.getAppointment().set(appointmentIndex, currentAppointment);
                                            
                                    System.out.println("");
                                    System.out.println("**********************************************************************************************");
                                    System.out.println("");
                                    System.out.println("Your consultancy time has been booked successfully!");
                                } else {
                                    System.out.println("Sorry wrong option selected. Please try again!");
                                }
                            }
                                    
                        }
                    }
                } else {
                    System.out.println("Sorry wrong option entered. Please try again!");
                }
            } else if (option1 == 3) {
                s.nextLine();
                System.out.println("Enter your name: ");
                String name = s.nextLine();
                System.out.println("Enter your address: ");
                String address = s.nextLine();
                System.out.println("Enter your telephone number: ");
                String telephoneNumber = s.nextLine();
                Person newPatient = new Person(rand.nextInt(1100), name, address, telephoneNumber);
                database.addPatient(newPatient);
                System.out.println("");
                System.out.println("**********************************************************************************************");
                System.out.println("");
                System.out.println("You have been successfully added to the patient list. Below are the details. Use the ID to use the services!");
                newPatient.toPrint();
            }else {
                System.out.println("Sorry wrong option entered. Please try again!");
            }
        } while(!exit);
    
        System.out.println("");
        System.out.println("**********************************************************************************************");
        System.out.println("");
        System.out.println("List of all treatment appointment!");

        database.listPhysiciansWithBookedTreatment();
        
        System.out.println("");
        System.out.println("**********************************************************************************************");
        System.out.println("");
        System.out.println("List of all consulting appointment!");
        
        database.listPhysiciansWithBookedConsulting();
        
        System.out.println("");
        System.out.println("**********************************************************************************************");
        System.out.println("");
        System.out.println("List of all appointment patient booked!");
        
        database.listPatientsAppointment();
    } 
}
