package javacoursework;

import java.util.ArrayList;

public class Database {
    private ArrayList<Physicians> physicians;
    private ArrayList<Person> patients;
    ArrayList<Expertise> expertises;
    
    public Database() {
        physicians = new ArrayList<Physicians>();
        patients = new ArrayList<Person>();
        expertises = new ArrayList<Expertise>();
    }
    
    public void addPhysician(Physicians p) {
        physicians.add(p);
    }
    
    public void addPatient(Person p) {
        patients.add(p);
    }
    
    public void addExpertise(Expertise e) {
        expertises.add(e);
    }
    
    public void listPhysician() {
        for (int i = 0; i < physicians.size(); i++) {
            Physicians p = physicians.get(i);
            p.print();
        }
    }
    
    public void listPhysiciansWithBookedTreatment() {
        int index = 1;
        for (int i = 0; i < physicians.size(); i++) {
            Physicians p = physicians.get(i);
            ArrayList<Appointment> appointments = p.getAppointment();
            for (int j = 0; j < appointments.size(); j++) {
                Appointment appointment = appointments.get(j);
                if (appointment.getAvailability().equals("No") && !appointment.getTreatment().getName().equals("Consulting")) {
                    System.out.println(index + ". Physician name: " + p.getFullName() + " | Treatment name with room: " + appointment.toString());
                    index++;
                }
            }
        }
    }
    
    public void listPhysiciansWithBookedConsulting() {
        int index = 1;
        for (int i = 0; i < physicians.size(); i++) {
            Physicians p = physicians.get(i);
            ArrayList<Appointment> appointments = p.getAppointment();
            for (int j = 0; j < appointments.size(); j++) {
                Appointment appointment = appointments.get(j);
                if (appointment.getAvailability().equals("No") && appointment.getTreatment().getName().equals("Consulting")) {
                    System.out.println(index + ". Physician name: " + p.getFullName() + " | Treatment name with room: " + appointment.toString());
                    index++;
                }
            }
        }
    }
    
    public void listExpertise() {
        for (int i = 0; i < expertises.size(); i++) {
            Expertise e = expertises.get(i);
            e.toPrint();
        }
    }
    
    public void listPatientsAppointment() {
        int index = 1;
        for (int i = 0; i < physicians.size(); i++) {
            Physicians p = physicians.get(i);
            ArrayList<Appointment> appointments = p.getAppointment();
            for (int j = 0; j < appointments.size(); j++) {
                Appointment appointment = appointments.get(j);
                if (appointment.getAvailability().equals("No") && !appointment.getTreatment().getName().equals("Consulting")) {
                    System.out.println(index + appointment.toStringDetail());
                    index++;
                }
            }
        }
    }
    
    public void listAppointmentByPatient(int patientId) {
        int index = 1;
    
        for (int i = 0; i < physicians.size(); i++) {
            Physicians p = physicians.get(i);
            
            ArrayList<Appointment> appointments = p.getAppointment();
            
            for (int j = 0; j < appointments.size(); j++) {
                Appointment appointment = appointments.get(j);
                
                if (appointment.getPatient().getIDNumber() == patientId) {
                    System.out.println(index + " " + appointment.toStringDetail());
                    index++;
                }
            }
        }
    }
    
    public int checkPatientId(int id) {
        for (int i = 0; i < patients.size(); i++) {
            Person p = patients.get(i);
            int idNumber = p.getIDNumber();
            
            if (idNumber == id) {
                return i;
            }
        }
        
        return -1;
    }
    
    public int checkExpertiseId(int id) {
        for (int i = 0; i < expertises.size(); i++) {
            Expertise e = expertises.get(i);
            int idNumber = e.getIDNumber();
            
            if (idNumber == id) {
                return i;
            }
        }
        
        return -1;
    }
    
    public int checkPhysicianId(int id) {
        for (int i = 0; i < physicians.size(); i++) {
            Physicians p = physicians.get(i);
            int idNumber = p.getIDNumber();
            
            if (idNumber == id) {
                return i;
            }
        }
        
        return -1;
    }
    
    public int checkAppointmentId(int id, Physicians p) {
        ArrayList<Appointment> appointments = p.getAppointment();
        
        for (int i = 0; i < appointments.size(); i++) {
            Appointment appointment = appointments.get(i);
            
            if (appointment.getIDNumber() == id) {
                return i;
            }
        }
        
        return -1;
    }
    
    public int checkAppointmentById(int id) {
        for (int j = 0; j <= physicians.size(); j++) {
            Physicians p = physicians.get(j);
            ArrayList<Appointment> appointments = p.getAppointment();
        
            for (int i = 0; i < appointments.size(); i++) {
                Appointment appointment = appointments.get(i);

                if (appointment.getIDNumber() == id) {
                    appointment.setStatus("Attended");
                    p.getAppointment().set(i, appointment);
                    return i;
                }
            }
        }
        
        return -1;
    }
    
    public int cancelAppointment(int id) {
        for (int j = 0; j <= physicians.size(); j++) {
            Physicians p = physicians.get(j);
            ArrayList<Appointment> appointments = p.getAppointment();
        
            for (int i = 0; i < appointments.size(); i++) {
                Appointment appointment = appointments.get(i);

                if (appointment.getIDNumber() == id) {
                    appointment.setStatus("Not attended");
                    appointment.setAvailability("Yes");
                    appointment.setPatient(new Person());
                    p.getAppointment().set(i, appointment);
                    return i;
                }
            }
        }
        
        return -1;
    }
    
    public Person getPatientByIndex(int i) {
        return patients.get(i);
    }
    
    public Expertise getExpertiseByIndex(int i) {
        return expertises.get(i);
    }
    
    public Physicians getPhysicianByIndex(int i) {
        return physicians.get(i);
    }
    
    public Appointment getAppointmentByIndex(int i, Physicians p) {
        ArrayList<Appointment> appointments = p.getAppointment();
        return appointments.get(i);
    }
    
    public void getPhysiciansByExpertise(Expertise e) {
        ArrayList<Physicians> expertisePhysicians = new ArrayList<Physicians>();
        
        for (int i = 0; i < physicians.size(); i++) {
            Physicians p = physicians.get(i);
            
            for (int j = 0; j < p.getExpertise().size(); j++) {
                Expertise expertise = p.getExpertise().get(j);
                if (expertise.getIDNumber() == e.getIDNumber()) {
                    expertisePhysicians.add(p);
                }
            }
        }
        System.out.println(expertisePhysicians.size());
        for (int i = 0; i < expertisePhysicians.size(); i++) {
            Physicians p = expertisePhysicians.get(i);
            p.print();
        }
    }
}
