package javacoursework;

public class Treatment {
    private String name;
    private Room room;
    
    public Treatment() {
    }
    
    public Treatment(String n, Room r) {
        name = n;
        room = r;
    }
    
    public String getName() {
        return name;
    }
    
    public String toString() {
        return (name + "( " + room.getName() + " )");
    }
    
    public void toPrint() {
        System.out.println(name + "( " + room.getName() + " )");
    }
}
