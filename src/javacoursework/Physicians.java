package javacoursework;
import java.util.ArrayList;

public class Physicians extends Person {
    private ArrayList<Expertise> expertise;
    private ArrayList<Appointment> appointments;
    
    public Physicians(int idNumber, String fullName, String address, String telephoneNumber) {
        super(idNumber, fullName, address, telephoneNumber);
        expertise = new ArrayList<Expertise>();
        appointments = new ArrayList<Appointment>();
    }
    
    public void addAppointment(Appointment a) {
        appointments.add(a);
    }
    
    public void addExpertise(Expertise e) {
        expertise.add(e);
    }
    
    public ArrayList<Expertise> getExpertise() {
        return expertise;
    }
    
    public ArrayList<Appointment> getAppointment() {
        return appointments;
    }

    public void printExpertise() {
        System.out.println("Expertise           :");
        for(int i = 0; i < expertise.size(); i++) {
            Expertise e = expertise.get(i);
            e.toPrintDetails();
        }
    }

    public void printAppointment() {
        System.out.println("Appointments        :");
        for(int i = 0; i < appointments.size(); i++) {
            Appointment a = appointments.get(i);
            a.toPrint();
        }
    }
    
    public void print() {
        System.out.println("****************Physician********************");
        super.toPrint();
        printExpertise();
        printAppointment();
    }
}
