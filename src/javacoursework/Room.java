package javacoursework;

public class Room {
    private String name;
    
    public Room(String n) {
        name = n;
    }
    
    public String getName() {
        return name;
    }
}
