package javacoursework;

public class Expertise {
    private int idNumber;
    private String name;
    private Treatment treatment;
    
    public Expertise(int idn, String n, Treatment t) {
        idNumber = idn;
        name = n;
        treatment = t;
    }
    
    public int getIDNumber() {
        return idNumber;
    }
    
    public Treatment getTreatment() {
        return treatment;
    }
    
    public void toPrint() {
        System.out.print("    ID number       :   " + idNumber);
        System.out.print("    Name            :   " + name);
        System.out.println("");
    }
    
    public void toPrintDetails() {
        System.out.println("    ID number       :   " + idNumber);
        System.out.print("    Name            :   " + name);
        System.out.print("    Treatments      :   " );
        toPrintTreatment();
        System.out.println("");
    }
    
    public void toPrintTreatment() {
        treatment.toPrint();
    }
}
