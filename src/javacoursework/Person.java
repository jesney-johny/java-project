package javacoursework;

public class Person {
    private int idNumber;
    private String fullName;
    private String address;
    private String telephoneNumber;
    
    public Person() {
        idNumber = 000;
        fullName = " ";
        address = " ";
        telephoneNumber = " ";
    }
    
    public Person(String fn) {
        idNumber = 100;
        fullName = fn;
        address = " ";
        telephoneNumber = " ";
    }
    
    public Person(int idN, String fn, String add, String tp) {
        idNumber = idN;
        fullName = fn;
        address = add;
        telephoneNumber = tp;
    }
    
    public int getIDNumber() {
        return idNumber;
    }
    
    public String getFullName() {
        return fullName;
    }
    
    public String getAddress() {
        return address;
    }
    
    public String getTelephoneNumber() {
        return telephoneNumber;
    }
    
    public void toPrint() {
        System.out.println("ID Number           :   " + getIDNumber());
        System.out.println("FullName            :   " + getFullName());
        System.out.println("Address             :   " + getAddress());
        System.out.println("Telephone Number    :   " + getTelephoneNumber());
    }
}
