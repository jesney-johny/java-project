package javacoursework;

public class Appointment {
    private int idNumber;
    private String date;
    private String time;
    private String available;
    private String status;
    private Person patient;
    private Treatment treatment;
    
    public Appointment(int idn, String d, String t) {
        idNumber = idn;
        date = d;
        time = t;
        available = "Yes";
        status = "Not attended";
        patient = new Person();
        treatment = new Treatment();
    }
    
    public int getIDNumber() {
        return idNumber;
    }
    
    public String getAvailability() {
        return available;
    }
    
    public Treatment getTreatment() {
        return treatment;
    }
    
    public Person getPatient() {
        return patient;
    }
    
    public void setPatient(Person p) {
        patient = p;
    }
    
    public void setAvailability(String a) {
        available = a;
    }
    
    public void setStatus(String a) {
        status = a;
    }
    
    public void setTreatment(Treatment t) {
        treatment = t;
    }
    
    public void toPrint() {
        System.out.print("    " + idNumber + "  " + date + " " + time + " ");
        System.out.println(patient.getFullName());
        if (available.equals("Yes")) {
            System.out.print("Available");
        } else {
            System.out.print("Booked");
        }
        System.out.println("");
    }
    
    public String toString() {
        return (treatment.toString() + " | Patient name: " + patient.getFullName() + " | Time: " + time);
    }
    
    public String toStringDetail() {
        return ("Id: " + idNumber + " | Patient name: " + patient.getFullName() + " | Time: " + time + " | Status: " + status);
    }
}
